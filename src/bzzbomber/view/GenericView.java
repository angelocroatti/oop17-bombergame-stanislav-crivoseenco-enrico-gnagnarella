package bzzbomber.view;

/**
 * Interface for every separate window of the game.
 */
public interface GenericView {

    /**
     * Needs to be called every time somebody wants a view appear on the screen.
     */
    void show();

}
